# origin

Ce dépôt permet de configurer une instance complète avec tous les services sur un seul serveur.
Le site internet peut être déporté ailleur si nécessaire.

## Modules déployés

Plusieurs modules sont déployés sur le serveur:

- git et gitweb (permettant d'accéder à un dépôt git en https)
- matrix -> Chat chiffré de bout en bout
- jitsi -> Visio (configuré en lien avec Matrix)
- resurrection -> Le référentiel de donnée permettant de gérer tous les utilisateurs + contient le ldap
- zammad -> Gestionnaire de ticket pour répondre au mail de manière performante
- wordpress -> Le site internet

Les modules sont déployés via ansible.


## Déploiement via ansible

Pour cela, vous devez avoir un accès SSH fonctionnel à la machine à configurer.
Vous devez pouvoir vous connecter via SSH sans mot de passe (clé publique) avec un utilisateur nommé `rdw` et l'utilisateur doit pouvoir passer en sudo sans mot de passe (configuration typique en mode cloud).

Pour que l'utilisateur `rdw` puisse se connecter en sudo sans mot de passe, vous pouvez executer la commande suivante:

```
sudo adduser rdw
sudo sh -c 'echo "rdw ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers'
```

Ensuite en tant que l'utilisateur `rdw`, ajouter votre clé ssh:

```
sudo su rdw
mkdir /home/rdw/.ssh
echo "ma_cle" >> /home/rdw/.ssh/authorized_keys
```

SSH doit être configuré sur le port 549 pour raison de sécurité.
Pour cela:

```
sudo sh -c 'echo "Port 549" >> /etc/ssh/sshd_config'
sudo service ssh restart
```


1. Installez ansible
2. Modifiez ou créez le fichier /etc/ansible/hosts avec le contenu suivant:

```
[website]
site_web_dns.com:549

[services]
services_dns.com:549
```

3. Lancez la commande `ansible-playbook playbook-init-server.yml`
